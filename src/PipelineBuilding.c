// File: PipelineBuilding.c
// Brief: This file iterates through all the combinations of pipeline stages 
//        in different NumOfStage and StagedFanin(stored in StagedFanin array).
// Compile: gcc PipelineBuilding.c -o PipelineBuilding -std=c99 -lm


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#define MIN_FANIN 2
#define MAX_FANIN 512

#define MIN_BITWIDTH 1
#define MAX_BITWIDTH 32

bool isPowerOfTwo (int x) {
  return !(x & (x - 1));
}

void makeFilename(char *Filename, char *ModuleName, int Bitwidth, int Fanin, int NumOfStage,
                  int *StagedFanin) {
  sprintf(Filename, "mux_%dto1_%dbit_%dstages", Fanin, Bitwidth, NumOfStage);
  char Appendix [100];
  for (int i = 1; i <= NumOfStage; i++) {
    sprintf(Appendix,"_%d", StagedFanin[i]);
    strcat(Filename, Appendix);
  }
  strcpy(ModuleName, Filename);
  strcat(Filename, ".v");
  puts(Filename);
}

bool VerilogPrinter(char *Path, char *Filename, char *ModuleName,
		    int Bitwidth, int Fanin, int NumOfStage, 
		    int *StagedFanin, int *NumOfStagedMux) {
  int selBitWidth = ceil(log2(Fanin));
  int StagedselBitWidth[NumOfStage + 1]; 
  FILE *fp = NULL;
  char FullFilename [200];
  char BitDir [100];
  sprintf(BitDir, "%d_bit/", Bitwidth);
  strcpy(FullFilename, Path);
  strcat(FullFilename, BitDir);
  strcat(FullFilename, Filename);
  fp = fopen(FullFilename, "w");
  if (fp != NULL) {
    // print ports of Module
    fprintf(fp, "// File: %s\n\n", Filename);
    fprintf(fp, "module %s (\n", ModuleName);
    fprintf(fp, "  input clk,\n");
    fprintf(fp, "  input [%d:0] in,\n", (Fanin * Bitwidth) - 1);
    fprintf(fp, "  input [%d:0] sel,\n", selBitWidth - 1);
    fprintf(fp, "  output [%d:0] out\n", Bitwidth - 1);
    fprintf(fp, ");\n\n");

    // print wires connecting stages 
    for (int i = 1; i <= NumOfStage; i++) {
      StagedselBitWidth[i] = ceil(log2(StagedFanin[i]));
      fprintf(fp, "wire [%d:0] st%d_to_st%d;\n", 
              NumOfStagedMux[i] * Bitwidth - 1, i, i + 1);
    }
    
    // print instantiation of stages
    for (int i = 1; i <= NumOfStage; i++) {
      int MSB = selBitWidth;
      for (int j = 1; j < i; j++) {
        MSB = MSB - StagedselBitWidth[j];
      }
      fprintf(fp, "st%d st%d_inst (\n", i, i);
      fprintf(fp, "  .clk(clk),\n");
      if (i == 1) {
        fprintf(fp, "  .in(in),\n");
	fprintf(fp, "  .sel(sel[%d:%d]),\n", selBitWidth - 1, 
		selBitWidth  - StagedselBitWidth[i]);
	// add if for NumOfStage == 1
        fprintf(fp, "  .out(st%d_to_st%d)\n", i, i + 1);
      }
      else if (i == NumOfStage) {
        fprintf(fp, "  .in(st%d_to_st%d),\n", i - 1, i);
	fprintf(fp, "  .sel(sel[%d:%d]),\n", MSB - 1, 0);
        fprintf(fp, "  .out(st%d_to_st%d)\n", i, i + 1);
        // fprintf(fp, "  .out(out)\n");
      }
      else {
        fprintf(fp, "  .in(st%d_to_st%d),\n", i - 1, i);
	fprintf(fp, "  .sel(sel[%d:%d]),\n", MSB - 1, MSB - StagedselBitWidth[i]);
        fprintf(fp, "  .out(st%d_to_st%d)\n", i, i + 1);
      }
      fprintf(fp, ");\n\n");
    }
    // add dff_*bit_out
    fprintf(fp, "dff_%dbit dff_%dbit_out (\n", Bitwidth, Bitwidth);
    fprintf(fp, "  .clk(clk),\n");
    fprintf(fp, "  .d(st%d_to_st%d),\n", NumOfStage, NumOfStage + 1);
    fprintf(fp, "  .q(out)\n");
    fprintf(fp, ");\n\n");
    fprintf(fp, "endmodule // end of %s\n\n", ModuleName);

    // print definitions of stages
    for (int i = 1; i <= NumOfStage; i++) {
      // ports definition
      fprintf(fp, "module st%d (\n", i);
      fprintf(fp, "  input clk,\n");
      fprintf(fp, "  input [%d:0] in,\n", StagedFanin[i] * NumOfStagedMux[i] * Bitwidth- 1);
      fprintf(fp, "  input [%d:0] sel,\n", StagedselBitWidth[i] - 1);
      fprintf(fp, "  output [%d:0] out\n", NumOfStagedMux[i] * Bitwidth - 1);
      fprintf(fp, ");\n\n");
      // logic in a stage
      fprintf(fp, "wire [%d:0] dff_to_mux;\n", StagedFanin[i] * NumOfStagedMux[i] * Bitwidth - 1);
      fprintf(fp, "wire [%d:0] sel_to_mux;\n", StagedselBitWidth[i] - 1);
      fprintf(fp, "genvar mux_i, fanin_i, sel_i;\n");
      fprintf(fp, "generate\n");
      fprintf(fp, "  for(mux_i = 1; mux_i <= %d; mux_i = mux_i + 1)\n", NumOfStagedMux[i]);
      fprintf(fp, "    begin : MULTI_MUX_INST\n");
      // generate mux instances
      fprintf(fp, "      mux_%dto1_%dbit mux_%dto1_%dbit_inst (\n", StagedFanin[i], Bitwidth, StagedFanin[i], Bitwidth);
      fprintf(fp, "        .d(dff_to_mux[mux_i * %d - 1:mux_i * %d - %d]),\n", StagedFanin[i] * Bitwidth, StagedFanin[i] * Bitwidth, StagedFanin[i] * Bitwidth);
      fprintf(fp, "        .sel(sel_to_mux),\n");
      fprintf(fp, "        .q(out[mux_i * %d - 1: mux_i * %d - %d])\n", Bitwidth, Bitwidth, Bitwidth);
      fprintf(fp, "      );\n");
      // generate dff_*bit instances
      fprintf(fp, "      for(fanin_i = 1; fanin_i <= %d; fanin_i = fanin_i + 1)\n", StagedFanin[i]);
      fprintf(fp, "        begin : MULTI_DFF_INST\n");
      fprintf(fp, "          dff_%dbit dff_%dbit_inst (\n", Bitwidth, Bitwidth);
      fprintf(fp, "            .clk(clk),\n");
      fprintf(fp, "            .d(in[(fanin_i - 1) * %d + (mux_i - 1) * %d * %d + %d - 1 : (fanin_i - 1) * %d + (mux_i - 1) * %d * %d]),\n", 
		      Bitwidth, StagedFanin[i], Bitwidth, Bitwidth, 
		      Bitwidth, StagedFanin[i], Bitwidth);
      // fprintf(fp, "            .q(dff_to_mux[(fanin_i - 1) + ((mux_i - 1) * %d)])\n", StagedFanin[i]);
      fprintf(fp, "            .q(dff_to_mux[(fanin_i - 1) * %d + (mux_i - 1) * %d * %d + %d - 1 : (fanin_i - 1) * %d + (mux_i - 1) * %d * %d])\n", 
		      Bitwidth, StagedFanin[i], Bitwidth, Bitwidth, 
		      Bitwidth, StagedFanin[i], Bitwidth);
      fprintf(fp, "          );\n");
      fprintf(fp, "        end\n");
      fprintf(fp, "    end\n");
      // generate sel_dff instances
      fprintf(fp, "  for(sel_i = 1; sel_i <= %d; sel_i = sel_i + 1)\n", StagedselBitWidth[i]);
      fprintf(fp, "    begin : MULTI_SEL_INST\n");
      fprintf(fp, "      dff_1bit sel_dff_1bit_inst (\n");
      fprintf(fp, "        .clk(clk),\n");
      fprintf(fp, "        .d(sel[sel_i - 1]),\n");
      fprintf(fp, "        .q(sel_to_mux[sel_i - 1])\n");
      fprintf(fp, "      );\n");
      fprintf(fp, "    end\n");
      fprintf(fp, "endgenerate\n");
      fprintf(fp, "endmodule // end of st%d\n\n", i);
    }

    fprintf(fp, "// end of file\n");
    fclose(fp);
    return 0;
  }
  else {
    perror("Cannot open the file!\n");
    return 1;
  }
}

int main() {
  // scan bitwidth of a mux
  for (int bitwidth_i = MIN_BITWIDTH; bitwidth_i <= MAX_BITWIDTH; bitwidth_i *= 2 ) {
    if ((bitwidth_i == 2) || (bitwidth_i == 4)) 
      continue;
    int Fanin;
    // Given a bitwidth of mux, scan Fanin
    for (Fanin = MIN_FANIN; Fanin <= MAX_FANIN; Fanin++) {
      int MaxNumOfStage; 
      MaxNumOfStage = ceil(log2(Fanin));
      if (isPowerOfTwo(Fanin)) {
        printf("=======\nFound Power of Two: %d\n", Fanin); 
        // given a NumOfStage, scan all the combinations of pipelined stages
        for (int NumOfStage = 1; NumOfStage <= MaxNumOfStage; NumOfStage++) {
          char Filename[100];
          char ModuleName[100];
          char Path[] = "/home/liwei/repos/mux_scan/verilog/";
          int StagedFanin[NumOfStage + 1]; 
          int NumOfStagedMux[NumOfStage + 1]; 
          for (int StageIdx = 1; StageIdx <= NumOfStage; StageIdx++) {
            if (StageIdx == 1) {
              printf("Fanin = %d, NumOfStage = %d\n", Fanin, NumOfStage);
              StagedFanin[StageIdx] = Fanin >> (NumOfStage - 1);
              NumOfStagedMux[StageIdx] = Fanin / StagedFanin[StageIdx];
              printf("StagedFanin[%d] = %d, NumOfStagedMux[%d] = %d\n", 
                     StageIdx, StagedFanin[StageIdx], 
		     StageIdx, NumOfStagedMux[StageIdx]);
            }
            else {
              StagedFanin[StageIdx] = 2;  
              NumOfStagedMux[StageIdx] = NumOfStagedMux[StageIdx - 1] / 2;
              printf("StagedFanin[%d] = %d, NumOfStagedMux[%d] = %d\n", 
                      StageIdx, StagedFanin[StageIdx], 
		      StageIdx, NumOfStagedMux[StageIdx]);
            }
          }
          makeFilename(Filename, ModuleName, bitwidth_i, Fanin, NumOfStage, StagedFanin);
          // write to .v file 
          VerilogPrinter(Path, Filename, ModuleName, bitwidth_i, Fanin, NumOfStage, StagedFanin, NumOfStagedMux);
          int GainedStageIdx = 2;
          // while loop for each combination given a NumOfStage
          while ((StagedFanin[1] > StagedFanin[2]) && (NumOfStage != 1)) {
            printf("------\n"); 
            StagedFanin[1] = StagedFanin[1] >> 1; 
            NumOfStagedMux[1] = NumOfStagedMux[1] * 2;
            StagedFanin[GainedStageIdx] = StagedFanin[GainedStageIdx] << 1; 
            if (StagedFanin[1] < StagedFanin[GainedStageIdx]) break;
            // recalculate the NumOfStagedMux
            for (int StageIdx = 2; StageIdx <= NumOfStage; StageIdx++) {
              NumOfStagedMux[StageIdx] = NumOfStagedMux[StageIdx - 1] / StagedFanin[StageIdx];
            }
            if (GainedStageIdx < NumOfStage) GainedStageIdx++;
            // print out the combination
            for (int StageIdx = 1; StageIdx <= NumOfStage; StageIdx++) {
              printf("StagedFanin[%d] = %d, NumOfStagedMux[%d] = %d\n", 
                StageIdx, StagedFanin[StageIdx], StageIdx, NumOfStagedMux[StageIdx]);
            }
            // name the .v file
            makeFilename(Filename, ModuleName, bitwidth_i, Fanin, NumOfStage, StagedFanin);
            // write to .v file 
            VerilogPrinter(Path, Filename, ModuleName, bitwidth_i, Fanin, NumOfStage, StagedFanin, NumOfStagedMux);
            if (StagedFanin[2] <= StagedFanin[GainedStageIdx]) GainedStageIdx = 2;
          } // end of while loop for each combination
        }
      } // end of checking only the power-of-two
      else {
        for (int NumOfStage = 1; NumOfStage <= MaxNumOfStage; NumOfStage++) {
        }
      }
    } // end of scanning Fanin
  } // end of scanning bitwidth
} // end of main()

// end of this file

