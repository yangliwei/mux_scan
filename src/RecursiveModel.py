#!/usr/bin/python
from __future__ import division
import os 
import MySQLdb 
from math import *

def isPowOfTwo(num):
  return num != 0 and ((num & (num - 1)) == 0)

def permuteNonPowOfTwo(NonPowFanin):
  global TempList 
  global MuxList
  LastExp = int(floor(log(NonPowFanin, 2)))
  ExtraFanin = NonPowFanin - (2 ** LastExp) + 1
  # generate combinations of LastExp
  if LastExp != 0:
    permutePowOfTwo(LastExp)
    TempList.append(MuxList[2 ** LastExp - 1])
  if isPowOfTwo(ExtraFanin):
    permutePowOfTwo(int(log(ExtraFanin, 2)))
    TempList.append(MuxList[ExtraFanin - 1])
    MuxList[Fanin - 1] = TempList
    TempList = [[ ]]
    del TempList[0]
    # return combinations appending to LastExp
    return
  else:
    permuteNonPowOfTwo(ExtraFanin)

# Exp is the logarithm of Fanin to base 2
def permutePowOfTwo(Exp):
  global MuxList
  global CurrStage
  global CurrExp 
  CurrStage += 1
  if Exp == 0:
    CombinationList = []
    for j in  range(1, CurrStage):
      CombinationList.append(StageTable["st%d" % (j)])
    MuxList[Fanin - 1].append(CombinationList)
    CurrStage = CurrStage - 2
    return MuxList[Fanin - 1]
  else:
    for i in range(1, Exp + 1):
      CurrExp = i
      StageTable["st%d" % (CurrStage)] = CurrExp
      permutePowOfTwo(Exp - i)

def permute(Fanin):
  global CurrExp
  if isPowOfTwo(Fanin):
    permutePowOfTwo(int(log(Fanin, 2)))
  else:
    permuteNonPowOfTwo(Fanin)

def AreaModel(AreaFanin, Bitwidth):
  global UsedALUT
  global UsedReg
  global UsedFmax
  global ALUTList
  global PercALUTList
  global RegList
  global PercRegList
  global Fanin
  if isPowOfTwo(AreaFanin):
    for Combination in MuxList[AreaFanin - 1]:
      # getFmax for a given combination
      ExpOfFmax = max(Combination)
      UsedFmax.append(getFmax((2 ** ExpOfFmax), Bitwidth))
      # modelling Area
      NumOfSubMux = []
      SubMuxFanin = []
      ALUT_Sum = 0
      Reg_Sum = 0
      NumOfStage = len(Combination)
      for stage_i in range(1, NumOfStage + 1):
        SubMuxFanin.append(2 ** Combination[stage_i - 1])
	if stage_i == 1:
	  NumOfSubMux.append(AreaFanin / SubMuxFanin[stage_i - 1])
	else:
          NumOfSubMux.append(NumOfSubMux[stage_i - 2] / SubMuxFanin[stage_i - 1])
	Reg_Sum += NumOfSubMux[stage_i - 1] * (getReg(SubMuxFanin[stage_i - 1], Bitwidth) - Combination[stage_i - 1] - Bitwidth) + Combination[stage_i - 1] # subtract Registers used on 'sel' 
	ALUT_Sum += NumOfSubMux[stage_i - 1] * getALUT(SubMuxFanin[stage_i - 1], Bitwidth) 
      UsedALUT.append(ALUT_Sum)
      # if Combination == MuxList[AreaFanin - 1][-1]:
      UsedReg.append(Reg_Sum + Bitwidth) # add the output Register for a Bitwidth
    ALUTList[AreaFanin - 1] = UsedALUT
    PercALUTList[AreaFanin - 1] = [float(x) / 58080 for x in UsedALUT]
    UsedALUT = []
    RegList[AreaFanin - 1] = UsedReg
    PercRegList[AreaFanin - 1] = [float(x) / 58080 for x in UsedReg]
    UsedReg = []
    FmaxList[AreaFanin - 1] = UsedFmax
    UsedFmax = []
  else:
    for Combination in MuxList[AreaFanin - 1]:
      ALUTList[AreaFanin - 1].append(ALUTList[2 ** sum(Combination[0]) - 1])
      PercALUTList[AreaFanin - 1].append(PercALUTList[2 ** sum(Combination[0]) - 1])
      RegList[AreaFanin - 1].append(RegList[2 ** sum(Combination[0]) - 1])
      PercRegList[AreaFanin - 1].append(PercRegList[2 ** sum(Combination[0]) - 1])
      FmaxList[AreaFanin - 1].append(FmaxList[2 ** sum(Combination[0]) - 1])

def getALUT(ALUTFanin, Bitwidth):
  return ALUT[Bitwidth][ALUTFanin] 

def getReg(RegFanin, Bitwidth):
  return Reg[Bitwidth][RegFanin] 

def getFmax(FmaxFanin, Bitwidth):
  return Fmax[Bitwidth][FmaxFanin] 

# solver functions
def getBestALUT(ALUTFanin, Bitwidth):
  global ALUTList
  BestALUT = 0 
  if isPowOfTwo(ALUTFanin):
    BestALUT = min(ALUTList[ALUTFanin - 1]) 
    return BestALUT
  else:
    for DecomposeStage in ALUTList[ALUTFanin - 1]:
      BestALUT += min(DecomposeStage)
    return BestALUT

def getBestReg(RegFanin, Bitwidth):
  global RegList
  BestReg = 0 
  if isPowOfTwo(RegFanin):
    BestReg = min(RegList[RegFanin - 1])
    return BestReg
  else:
    for DecomposeStage in RegList[RegFanin - 1]:
      BestReg += min(DecomposeStage) - Bitwidth # subtract the output register inbetween the stages
    BestReg += Bitwidth # add output register in the final stages
    return BestReg

def getBestFmax(FmaxFanin, Bitwidth):
  global FmaxList
  BestDecomposedFmax = []
  BestFmax = 0
  if isPowOfTwo(FmaxFanin):
    BestFmax = max(FmaxList[FmaxFanin - 1])
    return BestFmax
  else:
    for DecomposeStage in FmaxList[FmaxFanin - 1]:
      BestDecomposedFmax.append(max(DecomposeStage))
    BestFmax = max(BestDecomposedFmax)
    return BestFmax

# Dictionaries to store database for ALUT, Reg and Fmax
ALUT = {1:{}, 8:{}, 16:{}, 32:{}}
PercALUT = {1:{}, 8:{}, 16:{}, 32:{}}
Reg = {1:{}, 8:{}, 16:{}, 32:{}}
PercReg = {1:{}, 8:{}, 16:{}, 32:{}}
Fmax = {1:{}, 8:{}, 16:{}, 32:{}}
ClkPeriod = {1:{}, 8:{}, 16:{}, 32:{}}
# Initialize ALUT and Reg from MySQLdb
BitwidthList = [1, 8, 16, 32]
Bitwidth = 1
db = MySQLdb.connect("localhost", "liwei", "427427", "SingleStage_Mux")
cursor = db.cursor()
for Bitwidth in BitwidthList:
  sql = "SELECT * FROM %d_bit" % (Bitwidth)
  cursor.execute(sql)
  results = cursor.fetchall()
  for row in results:
    Fanin = row[0]
    ALUTs = row[1]
    Regs = row[2]
    Fmaxs = row[3]
    Period = row[4]
    # print "Fanin=%d, ALUTs=%d, Reg=%d, Fmax=%d, Period=%d" % (Fanin, ALUTs, Regs, Fmaxs, Period)
    ALUT[Bitwidth][Fanin] = ALUTs
    PercALUT[Bitwidth][Fanin] = ALUTs / 58080
    Reg[Bitwidth][Fanin] = Regs
    PercReg[Bitwidth][Fanin] = Regs / 58080
    Fmax[Bitwidth][Fanin] = Fmaxs
    ClkPeriod[Bitwidth][Fanin] = Period
# disconnect from server
db.close()


CurrStage = 0
CurrExp = 0
StageTable = {} # store a specific combination 
MuxList = [] # list to store all the 512 lists of combinations 
UsedALUT = []
ALUTList = []
PercALUTList = []
RegList = []
PercRegList = []
PercAreaList = []
FmaxList = []
UsedReg = [] 
UsedFmax = []
for i in range(1, 513):
  MuxList.append([])
  ALUTList.append([])
  PercALUTList.append([])
  RegList.append([])
  PercRegList.append([])
  PercAreaList.append([])
  FmaxList.append([])
TempList = [[ ]]
del TempList[0]

for Fanin in range(2, 513):
  permute(Fanin)
  AreaModel(Fanin, 32)
  if isPowOfTwo(Fanin):
    PercAreaList[Fanin - 1] = [a + b for a, b in zip(PercALUTList[Fanin - 1], PercRegList[Fanin - 1])]
  else:
    PercAreaList[Fanin - 1] = [[a + b for a, b in zip(SubList1, SubList2)] for SubList1, SubList2 in zip(PercALUTList[Fanin - 1], PercRegList[Fanin - 1])]


# print "Whole MuxList:>>>>>"
# print str(MuxList)
# 
# print "BestALUT = %d" % (getBestALUT(8, 32))
# print "Whole ALUTList:>>>>>"
# print str(ALUTList)
# print "Whole PercALUTList:>>>>>"
# print str(PercALUTList)
# print "BestReg = %d" % (getBestReg(8, 32))
# print "Whole RegList:>>>>>"
# print str(RegList)
# print "Whole PercRegList:>>>>>"
# print str(PercRegList)
# print "Whole PercAreaList:>>>>>"
# print str(PercAreaList)
# print "BestFmax = %f" % (getBestFmax(8, 32))
# print "Whole FmaxList:>>>>>"
# print str(FmaxList)

FileName = "Mux.lua"
FILE = open(FileName, 'w')
for Bitwidth in BitwidthList:
  # print Latency 
  FILE.write("FUs.Mux_%dbit = { Latencies = { N/A" % Bitwidth)
  for Latency in ClkPeriod[Bitwidth].values():
    FILE.write(", %.3f" % (Latency))
  FILE.write("},\n")
  # print ALUT
  FILE.write("                  ALUTs = { N/A")
  for LuaALUT in ALUT[Bitwidth].values():
    FILE.write(", %d" % (LuaALUT))
  FILE.write("},\n")
  # print Reg 
  FILE.write("                  Regs = { N/A")
  for LuaReg in Reg[Bitwidth].values():
    FILE.write(", %d" % (LuaReg))
  FILE.write("},\n")
  FILE.write("                }\n")
FILE.close()

