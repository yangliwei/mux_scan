#!/usr/bin/python
import os 
import math 

BitwidthList = [1, 8, 16, 32]
FaninList = []
for Bitwidth in BitwidthList:
  Path = "%d_bit/" % Bitwidth
  # os.mkdir(Path)
  for Fanin in range(2, 513):
    ModuleName = "mux_%dto1_%dbit_1stage" % (Fanin, Bitwidth) 
    FileName = ModuleName + ".v"
    FILE = open(Path + FileName, 'w')
    selBitwidth = math.ceil(math.log(Fanin, 2))

    # Mux ports
    FILE.write("module %s (\n" % ModuleName)
    FILE.write("  input clk,\n")
    FILE.write("  input [%d:0] in,\n" % (Fanin * Bitwidth - 1))
    FILE.write("  input [%d:0] sel,\n" % (selBitwidth - 1))
    FILE.write("  output [%d:0] out\n" % (Bitwidth - 1))
    FILE.write(");\n\n")
    
    # wires
    FILE.write("wire [%d:0] st1_to_out;\n\n" % (Bitwidth - 1))

    # st1 instances
    FILE.write("st1 st1_inst (\n")
    FILE.write("  .clk(clk),\n")
    FILE.write("  .in(in),\n")
    FILE.write("  .sel(sel),\n")
    FILE.write("  .out(st1_to_out)\n")
    FILE.write(");\n\n")

    # output dff instances
    FILE.write("dff_%dbit dff_%dbit_inst (\n" % (Bitwidth, Bitwidth))
    FILE.write("  .clk(clk),\n")
    FILE.write("  .d(st1_to_out),\n")
    FILE.write("  .q(out)\n")
    FILE.write(");\n\n")
    FILE.write("endmodule // end of %s\n\n" % ModuleName)

    # st1 module 
    FILE.write("module st1 (\n")
    FILE.write("  input clk,\n")
    FILE.write("  input [%d:0] in,\n" % (Fanin * Bitwidth - 1))
    FILE.write("  input [%d:0] sel,\n" % (selBitwidth - 1))
    FILE.write("  output [%d:0] out\n" % (Bitwidth - 1))
    FILE.write(");\n\n")
    FILE.write("genvar fanin_i, sel_i;\n")
    FILE.write("generate\n")
    FILE.write("  wire [%d:0] sel_to_mux;\n" % (selBitwidth - 1))
    FILE.write("  wire [%d:0] dff_to_mux;\n" % (Fanin * Bitwidth - 1))
    # generate dff to sel 
    FILE.write("  for(sel_i = 1; sel_i <= %d; sel_i = sel_i + 1)\n" % (selBitwidth))
    FILE.write("    begin : MULTI_SEL_INST\n")
    FILE.write("      dff_1bit sel_dff_1bit_inst (\n")
    FILE.write("        .clk(clk),\n")
    FILE.write("        .d(sel[sel_i - 1]),\n")
    FILE.write("        .q(sel_to_mux[sel_i - 1])\n")
    FILE.write("      );\n")
    FILE.write("    end\n")
    # generate dff to fanin
    FILE.write("  for(fanin_i = 1; fanin_i <= %d; fanin_i = fanin_i + 1)\n" % (Fanin))
    FILE.write("    begin : MULTI_DFF_INST\n")
    FILE.write("      dff_%dbit dff_%dbit_inst (\n" % (Bitwidth, Bitwidth))
    FILE.write("        .clk(clk),\n")
    FILE.write("        .d(in[(fanin_i - 1) * %d + %d - 1:(fanin_i - 1) * %d]),\n" % (Bitwidth, Bitwidth, Bitwidth))
    FILE.write("        .q(dff_to_mux[(fanin_i - 1) * %d + %d - 1:(fanin_i - 1) * %d])\n" % (Bitwidth, Bitwidth, Bitwidth))
    FILE.write("      );\n")
    FILE.write("    end\n")
    # generate mux 
    FILE.write("  mux_%dto1_%dbit mux_%dto1_%dbit_inst (\n" % (Fanin, Bitwidth, Fanin, Bitwidth))
    FILE.write("    .d(dff_to_mux),\n")
    FILE.write("    .sel(sel_to_mux),\n")
    FILE.write("    .q(out)\n")
    FILE.write("  );\n")
    FILE.write("endgenerate\n")
    FILE.write("endmodule // end of st1\n\n")

    # mux module 
    FILE.write("module mux_%dto1_%dbit (\n" % (Fanin, Bitwidth))
    FILE.write("  input [%d:0] d,\n" % (Fanin * Bitwidth - 1))
    FILE.write("  input [%d:0] sel,\n" % (selBitwidth - 1))
    FILE.write("  output [%d:0] q\n" % (Bitwidth - 1))
    FILE.write(");\n\n")
    FILE.write("wire [%d:0] d_array [0:%d];\n" % (Bitwidth - 1, Fanin - 1))
    for idx in range(1, Fanin + 1):
      FILE.write("assign d_array[%d] = d[%d:%d];\n" % (idx - 1, idx * Bitwidth - 1, idx * Bitwidth - Bitwidth))
    FILE.write("assign q = d_array[sel];\n")
    FILE.write("endmodule // end of mux\n\n")
    FILE.write("// end of file\n")

FILE.close()

