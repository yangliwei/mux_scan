#!/usr/bin/python

# JavaScript Object Notation
import json
# use MySQLdb instead of sqlite3
import MySQLdb
# import sqlite3
# Regular expression similar to Perl
import re
# Access to variables used by interpreter
import sys
# Scientific computing module
import numpy as np
# 2D plotting module
# Provides a MATLAB-like plotting framework
import matplotlib.pyplot as plt 
# PDF backend
import matplotlib.backends.backend_pdf as pdf # PDF backend

# draw the figure
def drawFigure(ydata):
  fig = plt.figure()
  width = 0.4
  legends = []
  legends.append('512to1_Mux')
  rect = plt.bar(NumOfStages[0:11], ydata[0:11], width, figure=fig)
  legend_rect = plt.legend(legends, ncol=len(legends), prop={'size':12}, loc='upper left')
  plt.ylabel('Fmax')
  plt.xlabel('Number of Stages')
  Area = "Area"
  # pp = pdf.PdfPages('%s_best.pdf' % Area)
  pp = pdf.PdfPages('Fmax_best.pdf')
  pp.savefig(fig, bbox_inches='tight')
  pp.close()

# Start here:
NumOfStages = [1, 2, 3, 4, 5, 6, 7, 8, 9]
util_perc = [59, 58, 58, 58, 59, 61, 67, 74, 87]
Fmax = [287.27, 388.5, 422.3, 415.97, 472.37, 474.83, 509.68, 478.01, 468.6]

drawFigure(Fmax)



