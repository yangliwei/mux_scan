// file: mux_scan.v

// mux_scan module
// module mux_scan (
module mux_scan (
  input clk,
        in1,
        in2,
        in3,
        in4,
  input [1:0] sel1,
        sel2,
  output out
);

wire q_dff1, q_dff2, q_mux;
wire mux_st1_st2, rest_st1_st2; 

st1 st1_inst (
  .clk(clk),
  .in1(in1),
  .in2(in2),
  .in3(in3),
  .in4(in4),
  .sel(sel1),
  .mux_out(mux_st1_st2),
  .rest_out(rest_st1_st2)
);

st2 st2_inst (
  .clk(clk),
  .in1(mux_st1_st2),
  .in2(rest_st1_st2),
  .sel(sel2),
  .mux_out(out)
);

endmodule // end of mux_scan module

// // mux_2to1 module
// module mux_2to1 (
//   input [1:0] d,
//   input sel,
//   output q
// );
// assign q = d[sel];
// endmodule // end of mux_2to1 module
// 
// mux_3to1 module
module mux_3to1 (
  input [2:0] d,
  input [1:0] sel,
  output q
);
assign q = d[sel];
endmodule // end of mux_3to1 module

// dff module, dff is a primitive
// module dff (input d,
// 		  clk,
// 	    output reg q
// 	   );
// always@(posedge clk)
//   q <= d;
// 
// endmodule // end of dff module

// st1: module for stage_1
module st1 (input clk, 
                  in1,
                  in2,
                  in3,
                  in4,
            input [1:0] sel,
            output mux_out,
                   rest_out
           );
wire [2:0] dff_in;
wire [2:0] mux_in;
assign dff_in[0] = in1;
assign dff_in[1] = in2;
assign dff_in[2] = in3;

dff dff_rest (
  .clk(clk),
  .d(in4),
  .q(rest_out)
); 
genvar i;
generate
  mux_3to1 mux_3to1_inst (
    .d(mux_in),
    .sel(sel),
    .q(mux_out)
  );

  for (i = 0; i < 3; i = i + 1)
    begin : FANIN_DFF
      dff dff_inst (
        .clk(clk),
        .d(dff_in[i]),
        .q(mux_in[i])
      ); 
    end 
endgenerate

endmodule // end of st1

// st2: module for stage_2
module st2 (input clk, 
                  in1,
                  in2,
                  sel,
            output mux_out 
           );
wire [1:0] dff_in;
wire [1:0] mux_in;
assign dff_in[0] = in1;
assign dff_in[1] = in2;

genvar i;

generate
  mux_2to1 mux_2to1_inst (
    .d(mux_in),
    .sel(sel),
    .q(mux_out)
  );

  for (i = 0; i < 2; i = i + 1)
    begin : FANIN_DFF
      dff dff_inst (
        .clk(clk),
        .d(dff_in[i]),
        .q(mux_in[i])
      );
    end 
endgenerate

endmodule // end of st2 




