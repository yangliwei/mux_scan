# This mux\_scan repository is for modeling the Mux to analytically get its characteristics
1. Scanning range of fanin is from 2-to-1 to 512-to-1, in 1-bit, 8-bit, 16-bit, 32-bit respecitively.
2. FPGA Device: Stratix IV, EP4SGX70HF35C2
3. Version: Quartus II 13.0, Subscription Edition. 

# Files
1. ./src/Single\_tables.sql: A MySQL script for creating the database for synthesis data of single-staged Muxs.
2. ./src/RecursiveModel.py: A Python script for building the model and implementing the solver functions for best permutation in terms of ALUT, Reg or Fmax respectively.

# Steps for importing MySQL database
1. Install MySQL, then create a database named: SingleStage\_Mux
2. Use SingleStage\_Mux database, and create tables: 1\_bit, 8\_bit, 16\_bit, 32\_bit using this format as an example:
CREATE TABLE 1\_bit (Fanin int, ALUTs int, Reg int, Fmax\_MHz float, Clk\_Period\_ns float)
3. SOURCE Single\_tables.sql, to populate the tables in the database;
# Steps for using the model for Mux
1. After the steps for MySQL are finished, install Python and MySQLdb module
2. ./RecursiveModel.py > log; Execute RecursiveModel.py and redirect output into log. This execution will build the MuxList for all permutation and ALUTList, RegList and FmaxList for all the analytical data computed according to the permutations in MuxList. And it will print the BestALUT, BestReg and BestFmax for a specific fanin. Just for a clearer demo, the range of fanin used in this example is from 2 to 16, and the fanin used for the solver functions for BestALUT, BestReg and BestFmax is 15 in 1\_bit;

