-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: mux_scan
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `mux_scan`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mux_scan` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mux_scan`;

--
-- Table structure for table `16_bit`
--

DROP TABLE IF EXISTS `16_bit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `16_bit` (
  `module` varchar(100) DEFAULT NULL,
  `comb_ALUT` int(11) DEFAULT NULL,
  `register` int(11) DEFAULT NULL,
  `fmax_MHz` varchar(50) DEFAULT NULL,
  `latency_cycles` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `16_bit`
--

LOCK TABLES `16_bit` WRITE;
/*!40000 ALTER TABLE `16_bit` DISABLE KEYS */;
INSERT INTO `16_bit` VALUES ('mux_128to1_16bit_2stages_16_8',688,2183,'525.76','2'),('mux_128to1_16bit_2stages_32_4',721,2119,'478.47','2'),('mux_128to1_16bit_2stages_64_2',688,2087,'426.44','2'),('mux_128to1_16bit_3stages_16_4_2',688,2215,'547.65','3'),('mux_128to1_16bit_3stages_32_2_2',721,2151,'467.51','3'),('mux_128to1_16bit_3stages_8_4_4',592,2375,'594.88','3'),('mux_128to1_16bit_4stages_16_2_2_2',656,2279,'534.19','4'),('mux_128to1_16bit_4stages_4_4_4_2',688,2727,'673.4','4'),('mux_128to1_16bit_4stages_8_4_2_2',592,2407,'558.04','4'),('mux_128to1_16bit_5stages_4_4_2_2_2',656,2791,'678.43','5'),('mux_128to1_16bit_5stages_8_2_2_2_2',528,2535,'585.48','5'),('mux_128to1_16bit_6stages_4_2_2_2_2_2',528,3047,'643.09','6'),('mux_128to1_16bit_7stages_2_2_2_2_2_2_2',16,4071,'643.5','7'),('mux_16to1_16bit_2stages_4_4',80,324,'844.59','2'),('mux_16to1_16bit_2stages_8_2',112,292,'696.86','2'),('mux_16to1_16bit_3stages_4_2_2',80,356,'868.81','3'),('mux_16to1_16bit_4stages_2_2_2_2',16,484,'844.59','4'),('mux_256to1_16bit_2stages_128_2',1393,4136,'344.35','2'),('mux_256to1_16bit_2stages_16_16',1360,4360,'482.16','2'),('mux_256to1_16bit_2stages_32_8',1457,4232,'420.88','2'),('mux_256to1_16bit_2stages_64_4',1360,4168,'417.89','2'),('mux_256to1_16bit_3stages_16_4_4',1360,4424,'498.01','3'),('mux_256to1_16bit_3stages_32_4_2',1457,4264,'413.39','3'),('mux_256to1_16bit_3stages_64_2_2',1360,4200,'415.8','3'),('mux_256to1_16bit_3stages_8_8_4',1232,4680,'548.25','3'),('mux_256to1_16bit_4stages_16_4_2_2',1360,4456,'498.26','4'),('mux_256to1_16bit_4stages_32_2_2_2',1425,4328,'437.45','4'),('mux_256to1_16bit_4stages_4_4_4_4',1360,5448,'573.39','4'),('mux_256to1_16bit_4stages_8_4_4_2',1200,4776,'534.19','4'),('mux_256to1_16bit_5stages_16_2_2_2_2',1296,4584,'473.26','5'),('mux_256to1_16bit_5stages_4_4_4_2_2',1360,5480,'582.75','5'),('mux_256to1_16bit_5stages_8_4_2_2_2',1168,4840,'546.75','5'),('mux_256to1_16bit_6stages_4_4_2_2_2_2',1296,5608,'594.53','6'),('mux_256to1_16bit_6stages_8_2_2_2_2_2',1040,5096,'545.26','6'),('mux_256to1_16bit_7stages_4_2_2_2_2_2_2',1040,6120,'567.21','7'),('mux_256to1_16bit_8stages_2_2_2_2_2_2_2_2',16,8168,'551.27','8'),('mux_32to1_16bit_2stages_16_2',176,549,'646.41','2'),('mux_32to1_16bit_2stages_8_4',208,581,'678.89','2'),('mux_32to1_16bit_3stages_4_4_2',176,677,'815.0','3'),('mux_32to1_16bit_3stages_8_2_2',208,613,'665.78','3'),('mux_32to1_16bit_4stages_4_2_2_2',144,741,'788.02','4'),('mux_32to1_16bit_5stages_2_2_2_2_2',16,997,'783.7','5'),('mux_4to1_16bit_2stages_2_2',16,98,'1142.86','2'),('mux_512to1_16bit_2stages_128_4',2769,8265,'331.24','2'),('mux_512to1_16bit_2stages_256_2',2736,8233,'325.1','2'),('mux_512to1_16bit_2stages_32_16',2640,8457,'395.57','2'),('mux_512to1_16bit_2stages_64_8',2736,8329,'372.02','2'),('mux_512to1_16bit_3stages_128_2_2',2769,8297,'325.41','3'),('mux_512to1_16bit_3stages_16_8_4',2768,8777,'433.28','3'),('mux_512to1_16bit_3stages_32_4_4',2640,8521,'383.88','3'),('mux_512to1_16bit_3stages_64_4_2',2736,8361,'362.71','3'),('mux_512to1_16bit_3stages_8_8_8',2480,9353,'487.33','3'),('mux_512to1_16bit_4stages_16_4_4_2',2736,8873,'466.85','4'),('mux_512to1_16bit_4stages_32_4_2_2',2640,8553,'396.67','4'),('mux_512to1_16bit_4stages_64_2_2_2',2704,8425,'369.55','4'),('mux_512to1_16bit_4stages_8_4_4_4',2384,9545,'494.8','4'),('mux_512to1_16bit_5stages_16_4_2_2_2',2704,8937,'434.4','5'),('mux_512to1_16bit_5stages_32_2_2_2_2',2576,8681,'394.01','5'),('mux_512to1_16bit_5stages_4_4_4_4_2',2736,10921,'521.65','5'),('mux_512to1_16bit_5stages_8_4_4_2_2',2384,9577,'469.04','5'),('mux_512to1_16bit_6stages_16_2_2_2_2_2',2576,9193,'436.49','6'),('mux_512to1_16bit_6stages_4_4_4_2_2_2',2704,10985,'533.9','6'),('mux_512to1_16bit_6stages_8_4_2_2_2_2',2320,9705,'461.04','6'),('mux_512to1_16bit_7stages_4_4_2_2_2_2_2',2576,11241,'531.35','7'),('mux_512to1_16bit_7stages_8_2_2_2_2_2_2',2064,10217,'465.12','7'),('mux_512to1_16bit_8stages_4_2_2_2_2_2_2_2',2064,12265,'504.03','8'),('mux_512to1_16bit_9stages_2_2_2_2_2_2_2_2_2',16,16361,'517.06','9'),('mux_64to1_16bit_2stages_16_4',336,1094,'592.42','2'),('mux_64to1_16bit_2stages_32_2',369,1062,'510.46','2'),('mux_64to1_16bit_2stages_8_8',432,1158,'619.96','2'),('mux_64to1_16bit_3stages_16_2_2',336,1126,'609.76','3'),('mux_64to1_16bit_3stages_4_4_4',336,1350,'764.53','3'),('mux_64to1_16bit_3stages_8_4_2',432,1190,'638.57','3'),('mux_64to1_16bit_4stages_4_4_2_2',336,1382,'751.31','4'),('mux_64to1_16bit_4stages_8_2_2_2',400,1254,'615.76','4'),('mux_64to1_16bit_5stages_4_2_2_2_2',272,1510,'699.3','5'),('mux_64to1_16bit_6stages_2_2_2_2_2_2',16,2022,'720.98','6'),('mux_8to1_16bit_2stages_4_2',48,163,'987.17','2'),('mux_8to1_16bit_3stages_2_2_2',16,227,'944.29','3');
/*!40000 ALTER TABLE `16_bit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1_bit`
--

DROP TABLE IF EXISTS `1_bit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1_bit` (
  `module` varchar(100) DEFAULT NULL,
  `comb_ALUT` int(11) DEFAULT NULL,
  `register` int(11) DEFAULT NULL,
  `fmax_MHz` varchar(50) DEFAULT NULL,
  `latency_cycles` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1_bit`
--

LOCK TABLES `1_bit` WRITE;
/*!40000 ALTER TABLE `1_bit` DISABLE KEYS */;
INSERT INTO `1_bit` VALUES ('mux_128to1_1bit_2stages_16_8',42,143,'789.89','2'),('mux_128to1_1bit_2stages_32_4',46,139,'585.14','2'),('mux_128to1_1bit_2stages_64_2',43,137,'623.83','2'),('mux_128to1_1bit_3stages_16_4_2',43,145,'704.72','3'),('mux_128to1_1bit_3stages_32_2_2',48,141,'610.5','3'),('mux_128to1_1bit_3stages_8_4_4',53,155,'817.66','3'),('mux_128to1_1bit_4stages_16_2_2_2',47,149,'733.68','4'),('mux_128to1_1bit_4stages_4_4_4_2',43,177,'996.02','4'),('mux_128to1_1bit_4stages_8_4_2_2',55,157,'800.0','4'),('mux_128to1_1bit_5stages_4_4_2_2_2',47,181,'1002.0','5'),('mux_128to1_1bit_5stages_8_2_2_2_2',55,165,'757.0','5'),('mux_128to1_1bit_6stages_4_2_2_2_2_2',39,197,'968.05','6'),('mux_128to1_1bit_7stages_2_2_2_2_2_2_2',7,261,'969.93','7'),('mux_16to1_1bit_2stages_4_4',5,24,'1132.5','2'),('mux_16to1_1bit_2stages_8_2',7,22,'888.1','2'),('mux_16to1_1bit_3stages_4_2_2',7,26,'1085.78','3'),('mux_16to1_1bit_4stages_2_2_2_2',7,34,'1086.96','4'),('mux_256to1_1bit_2stages_128_2',88,266,'496.28','2'),('mux_256to1_1bit_2stages_16_16',85,280,'706.21','2'),('mux_256to1_1bit_2stages_32_8',91,272,'561.17','2'),('mux_256to1_1bit_2stages_64_4',85,268,'552.18','2'),('mux_256to1_1bit_3stages_16_4_4',85,284,'715.31','3'),('mux_256to1_1bit_3stages_32_4_2',92,274,'580.38','3'),('mux_256to1_1bit_3stages_64_2_2',87,270,'561.17','3'),('mux_256to1_1bit_3stages_8_8_4',109,300,'744.6','3'),('mux_256to1_1bit_4stages_16_4_2_2',87,286,'674.31','4'),('mux_256to1_1bit_4stages_32_2_2_2',96,278,'616.14','4'),('mux_256to1_1bit_4stages_4_4_4_4',85,348,'930.23','4'),('mux_256to1_1bit_4stages_8_4_4_2',107,306,'742.39','4'),('mux_256to1_1bit_5stages_16_2_2_2_2',87,294,'704.23','5'),('mux_256to1_1bit_5stages_4_4_4_2_2',87,350,'907.44','5'),('mux_256to1_1bit_5stages_8_4_2_2_2',111,310,'730.99','5'),('mux_256to1_1bit_6stages_4_4_2_2_2_2',87,358,'970.87','6'),('mux_256to1_1bit_6stages_8_2_2_2_2_2',103,326,'711.74','6'),('mux_256to1_1bit_7stages_4_2_2_2_2_2_2',71,390,'929.37','7'),('mux_256to1_1bit_8stages_2_2_2_2_2_2_2_2',7,518,'881.06','8'),('mux_32to1_1bit_2stages_16_2',11,39,'897.67','2'),('mux_32to1_1bit_2stages_8_4',13,41,'837.52','2'),('mux_32to1_1bit_3stages_4_4_2',11,47,'1079.91','3'),('mux_32to1_1bit_3stages_8_2_2',15,43,'911.58','3'),('mux_32to1_1bit_4stages_4_2_2_2',15,51,'1077.59','4'),('mux_32to1_1bit_5stages_2_2_2_2_2',7,67,'1090.51','5'),('mux_4to1_1bit_2stages_2_2',3,8,'1366.12','2'),('mux_512to1_1bit_2stages_128_4',174,525,'460.62','2'),('mux_512to1_1bit_2stages_256_2',171,523,'446.83','2'),('mux_512to1_1bit_2stages_32_16',182,537,'573.07','2'),('mux_512to1_1bit_2stages_64_8',170,529,'560.22','2'),('mux_512to1_1bit_3stages_128_2_2',176,527,'466.2','3'),('mux_512to1_1bit_3stages_16_8_4',173,557,'614.63','3'),('mux_512to1_1bit_3stages_32_4_4',182,541,'547.05','3'),('mux_512to1_1bit_3stages_64_4_2',171,531,'539.08','3'),('mux_512to1_1bit_3stages_8_8_8',218,593,'681.66','3'),('mux_512to1_1bit_4stages_16_4_4_2',171,563,'658.33','4'),('mux_512to1_1bit_4stages_32_4_2_2',184,543,'589.97','4'),('mux_512to1_1bit_4stages_64_2_2_2',175,535,'531.07','4'),('mux_512to1_1bit_4stages_8_4_4_4',213,605,'702.74','4'),('mux_512to1_1bit_5stages_16_4_2_2_2',175,567,'654.02','5'),('mux_512to1_1bit_5stages_32_2_2_2_2',184,551,'520.83','5'),('mux_512to1_1bit_5stages_4_4_4_4_2',171,691,'838.22','5'),('mux_512to1_1bit_5stages_8_4_4_2_2',215,607,'708.22','5'),('mux_512to1_1bit_6stages_16_2_2_2_2_2',167,583,'637.35','6'),('mux_512to1_1bit_6stages_4_4_4_2_2_2',175,695,'809.72','6'),('mux_512to1_1bit_6stages_8_4_2_2_2_2',215,615,'661.38','6'),('mux_512to1_1bit_7stages_4_4_2_2_2_2_2',167,711,'834.72','7'),('mux_512to1_1bit_7stages_8_2_2_2_2_2_2',199,647,'665.34','7'),('mux_512to1_1bit_8stages_4_2_2_2_2_2_2_2',135,775,'816.99','8'),('mux_512to1_1bit_9stages_2_2_2_2_2_2_2_2_2',7,1031,'793.02','9'),('mux_64to1_1bit_2stages_16_4',21,74,'805.15','2'),('mux_64to1_1bit_2stages_32_2',24,72,'689.18','2'),('mux_64to1_1bit_2stages_8_8',26,78,'850.34','2'),('mux_64to1_1bit_3stages_16_2_2',23,76,'769.23','3'),('mux_64to1_1bit_3stages_4_4_4',21,90,'988.14','3'),('mux_64to1_1bit_3stages_8_4_2',27,80,'853.24','3'),('mux_64to1_1bit_4stages_4_4_2_2',23,92,'1006.04','4'),('mux_64to1_1bit_4stages_8_2_2_2',31,84,'858.37','4'),('mux_64to1_1bit_5stages_4_2_2_2_2',23,100,'1023.54','5'),('mux_64to1_1bit_6stages_2_2_2_2_2_2',7,132,'1075.27','6'),('mux_8to1_1bit_2stages_4_2',3,13,'1088.14','2'),('mux_8to1_1bit_3stages_2_2_2',7,17,'1215.07','3');
/*!40000 ALTER TABLE `1_bit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `32_bit`
--

DROP TABLE IF EXISTS `32_bit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `32_bit` (
  `module` varchar(100) DEFAULT NULL,
  `comb_ALUT` int(11) DEFAULT NULL,
  `register` int(11) DEFAULT NULL,
  `fmax_MHz` varchar(50) DEFAULT NULL,
  `latency_cycles` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `32_bit`
--

LOCK TABLES `32_bit` WRITE;
/*!40000 ALTER TABLE `32_bit` DISABLE KEYS */;
INSERT INTO `32_bit` VALUES ('mux_128to1_32bit_2stages_16_8',1376,4359,'482.16','2'),('mux_128to1_32bit_2stages_32_4',1441,4231,'414.25','2'),('mux_128to1_32bit_2stages_64_2',1376,4167,'406.34','2'),('mux_128to1_32bit_3stages_16_4_2',1376,4423,'503.27','3'),('mux_128to1_32bit_3stages_32_2_2',1441,4295,'445.04','3'),('mux_128to1_32bit_3stages_8_4_4',1184,4743,'507.61','3'),('mux_128to1_32bit_4stages_16_2_2_2',1312,4551,'495.29','4'),('mux_128to1_32bit_4stages_4_4_4_2',1376,5447,'608.64','4'),('mux_128to1_32bit_4stages_8_4_2_2',1184,4807,'510.2','4'),('mux_128to1_32bit_5stages_4_4_2_2_2',1312,5575,'575.37','5'),('mux_128to1_32bit_5stages_8_2_2_2_2',1056,5063,'488.52','5'),('mux_128to1_32bit_6stages_4_2_2_2_2_2',1056,6087,'567.21','6'),('mux_128to1_32bit_7stages_2_2_2_2_2_2_2',32,8135,'495.54','7'),('mux_16to1_32bit_2stages_4_4',160,644,'778.21','2'),('mux_16to1_32bit_2stages_8_2',224,580,'674.76','2'),('mux_16to1_32bit_3stages_4_2_2',160,708,'772.8','3'),('mux_16to1_32bit_4stages_2_2_2_2',32,964,'743.49','4'),('mux_256to1_32bit_2stages_128_2',2785,8264,'326.69','2'),('mux_256to1_32bit_2stages_16_16',2720,8712,'431.78','2'),('mux_256to1_32bit_2stages_32_8',2656,8456,'389.26','2'),('mux_256to1_32bit_2stages_64_4',2720,8328,'358.42','2'),('mux_256to1_32bit_3stages_16_4_4',2720,8840,'449.64','3'),('mux_256to1_32bit_3stages_32_4_2',2656,8520,'413.74','3'),('mux_256to1_32bit_3stages_64_2_2',2720,8392,'361.4','3'),('mux_256to1_32bit_3stages_8_8_4',2464,9352,'486.62','3'),('mux_256to1_32bit_4stages_16_4_2_2',2720,8904,'445.04','4'),('mux_256to1_32bit_4stages_32_2_2_2',2592,8648,'424.81','4'),('mux_256to1_32bit_4stages_4_4_4_4',2720,10888,'524.93','4'),('mux_256to1_32bit_4stages_8_4_4_2',2400,9544,'459.98','4'),('mux_256to1_32bit_5stages_16_2_2_2_2',2592,9160,'447.63','5'),('mux_256to1_32bit_5stages_4_4_4_2_2',2720,10952,'571.43','5'),('mux_256to1_32bit_5stages_8_4_2_2_2',2336,9672,'436.87','5'),('mux_256to1_32bit_6stages_4_4_2_2_2_2',2592,11208,'550.66','6'),('mux_256to1_32bit_6stages_8_2_2_2_2_2',2080,10184,'424.45','6'),('mux_256to1_32bit_7stages_4_2_2_2_2_2_2',2080,12232,'512.82','7'),('mux_256to1_32bit_8stages_2_2_2_2_2_2_2_2',32,16328,'521.92','8'),('mux_32to1_32bit_2stages_16_2',352,1093,'594.53','2'),('mux_32to1_32bit_2stages_8_4',416,1157,'613.87','2'),('mux_32to1_32bit_3stages_4_4_2',352,1349,'788.02','3'),('mux_32to1_32bit_3stages_8_2_2',416,1221,'615.38','3'),('mux_32to1_32bit_4stages_4_2_2_2',288,1477,'729.39','4'),('mux_32to1_32bit_5stages_2_2_2_2_2',32,1989,'690.13','5'),('mux_4to1_32bit_2stages_2_2',32,194,'963.39','2'),('mux_512to1_32bit_2stages_128_4',5537,16521,'321.23','2'),('mux_512to1_32bit_2stages_256_2',5472,16457,'304.97','2'),('mux_512to1_32bit_2stages_32_16',5280,16905,'388.5','2'),('mux_512to1_32bit_2stages_64_8',5472,16649,'347.95','2'),('mux_512to1_32bit_3stages_128_2_2',5537,16585,'317.06','3'),('mux_512to1_32bit_3stages_16_8_4',5536,17545,'415.28','3'),('mux_512to1_32bit_3stages_32_4_4',5280,17033,'391.24','3'),('mux_512to1_32bit_3stages_64_4_2',5472,16713,'355.11','3'),('mux_512to1_32bit_3stages_8_8_8',4704,18697,'422.3','3'),('mux_512to1_32bit_4stages_16_4_4_2',5472,17737,'411.52','4'),('mux_512to1_32bit_4stages_32_4_2_2',5280,17097,'378.07','4'),('mux_512to1_32bit_4stages_64_2_2_2',5408,16841,'358.55','4'),('mux_512to1_32bit_4stages_8_4_4_4',4768,19081,'415.97','4'),('mux_512to1_32bit_5stages_16_4_2_2_2',5408,17865,'396.35','5'),('mux_512to1_32bit_5stages_32_2_2_2_2',5152,17353,'386.1','5'),('mux_512to1_32bit_5stages_4_4_4_4_2',5472,21833,'472.37','5'),('mux_512to1_32bit_5stages_8_4_4_2_2',4768,19145,'455.58','5'),('mux_512to1_32bit_6stages_16_2_2_2_2_2',5152,18377,'434.4','6'),('mux_512to1_32bit_6stages_4_4_4_2_2_2',5408,21961,'474.83','6'),('mux_512to1_32bit_6stages_8_4_2_2_2_2',4640,19401,'438.79','6'),('mux_512to1_32bit_7stages_4_4_2_2_2_2_2',5152,22473,'509.68','7'),('mux_512to1_32bit_7stages_8_2_2_2_2_2_2',4128,20425,'426.62','7'),('mux_512to1_32bit_8stages_4_2_2_2_2_2_2_2',4128,24521,'478.01','8'),('mux_512to1_32bit_9stages_2_2_2_2_2_2_2_2_2',32,32713,'468.6','9'),('mux_64to1_32bit_2stages_16_4',672,2182,'543.48','2'),('mux_64to1_32bit_2stages_32_2',737,2118,'460.19','2'),('mux_64to1_32bit_2stages_8_8',608,2310,'584.11','2'),('mux_64to1_32bit_3stages_16_2_2',672,2246,'562.75','3'),('mux_64to1_32bit_3stages_4_4_4',672,2694,'655.31','3'),('mux_64to1_32bit_3stages_8_4_2',608,2374,'577.03','3'),('mux_64to1_32bit_4stages_4_4_2_2',672,2758,'663.57','4'),('mux_64to1_32bit_4stages_8_2_2_2',544,2502,'600.96','4'),('mux_64to1_32bit_5stages_4_2_2_2_2',544,3014,'659.2','5'),('mux_64to1_32bit_6stages_2_2_2_2_2_2',32,4038,'624.61','6'),('mux_8to1_32bit_2stages_4_2',96,323,'902.53','2'),('mux_8to1_32bit_3stages_2_2_2',32,451,'862.81','3');
/*!40000 ALTER TABLE `32_bit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `8_bit`
--

DROP TABLE IF EXISTS `8_bit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `8_bit` (
  `module` varchar(100) DEFAULT NULL,
  `comb_ALUT` int(11) DEFAULT NULL,
  `register` int(11) DEFAULT NULL,
  `fmax_MHz` varchar(50) DEFAULT NULL,
  `latency_cycles` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `8_bit`
--

LOCK TABLES `8_bit` WRITE;
/*!40000 ALTER TABLE `8_bit` DISABLE KEYS */;
INSERT INTO `8_bit` VALUES ('mux_128to1_8bit_2stages_16_8',344,1095,'615.01','2'),('mux_128to1_8bit_2stages_32_4',361,1063,'548.55','2'),('mux_128to1_8bit_2stages_64_2',344,1047,'483.33','2'),('mux_128to1_8bit_3stages_16_4_2',344,1111,'624.61','3'),('mux_128to1_8bit_3stages_32_2_2',361,1079,'509.42','3'),('mux_128to1_8bit_3stages_8_4_4',424,1191,'648.09','3'),('mux_128to1_8bit_4stages_16_2_2_2',328,1143,'598.8','4'),('mux_128to1_8bit_4stages_4_4_4_2',344,1367,'759.3','4'),('mux_128to1_8bit_4stages_8_4_2_2',424,1207,'606.8','4'),('mux_128to1_8bit_5stages_4_4_2_2_2',328,1399,'709.22','5'),('mux_128to1_8bit_5stages_8_2_2_2_2',392,1271,'634.52','5'),('mux_128to1_8bit_6stages_4_2_2_2_2_2',264,1527,'746.83','6'),('mux_128to1_8bit_7stages_2_2_2_2_2_2_2',8,2039,'735.84','7'),('mux_16to1_8bit_2stages_4_4',40,164,'1004.02','2'),('mux_16to1_8bit_2stages_8_2',56,148,'721.5','2'),('mux_16to1_8bit_3stages_4_2_2',40,180,'980.39','3'),('mux_16to1_8bit_4stages_2_2_2_2',8,244,'999.0','4'),('mux_256to1_8bit_2stages_128_2',697,2072,'405.84','2'),('mux_256to1_8bit_2stages_16_16',680,2184,'516.8','2'),('mux_256to1_8bit_2stages_32_8',729,2120,'485.91','2'),('mux_256to1_8bit_2stages_64_4',680,2088,'462.32','2'),('mux_256to1_8bit_3stages_16_4_4',680,2216,'539.37','3'),('mux_256to1_8bit_3stages_32_4_2',729,2136,'472.81','3'),('mux_256to1_8bit_3stages_64_2_2',680,2104,'457.67','3'),('mux_256to1_8bit_3stages_8_8_4',616,2344,'571.1','3'),('mux_256to1_8bit_4stages_16_4_2_2',680,2232,'548.25','4'),('mux_256to1_8bit_4stages_32_2_2_2',713,2168,'520.29','4'),('mux_256to1_8bit_4stages_4_4_4_4',680,2728,'629.33','4'),('mux_256to1_8bit_4stages_8_4_4_2',600,2392,'574.05','4'),('mux_256to1_8bit_5stages_16_2_2_2_2',648,2296,'529.38','5'),('mux_256to1_8bit_5stages_4_4_4_2_2',680,2744,'671.59','5'),('mux_256to1_8bit_5stages_8_4_2_2_2',584,2424,'560.54','5'),('mux_256to1_8bit_6stages_4_4_2_2_2_2',648,2808,'683.99','6'),('mux_256to1_8bit_6stages_8_2_2_2_2_2',520,2552,'598.09','6'),('mux_256to1_8bit_7stages_4_2_2_2_2_2_2',520,3064,'648.93','7'),('mux_256to1_8bit_8stages_2_2_2_2_2_2_2_2',8,4088,'636.13','8'),('mux_32to1_8bit_2stages_16_2',88,277,'685.87','2'),('mux_32to1_8bit_2stages_8_4',104,293,'741.29','2'),('mux_32to1_8bit_3stages_4_4_2',88,341,'918.27','3'),('mux_32to1_8bit_3stages_8_2_2',104,309,'703.73','3'),('mux_32to1_8bit_4stages_4_2_2_2',72,373,'865.05','4'),('mux_32to1_8bit_5stages_2_2_2_2_2',8,501,'903.34','5'),('mux_4to1_8bit_2stages_2_2',8,50,'1097.69','2'),('mux_512to1_8bit_2stages_128_4',1385,4137,'369.41','2'),('mux_512to1_8bit_2stages_256_2',1368,4121,'362.71','2'),('mux_512to1_8bit_2stages_32_16',1449,4233,'415.97','2'),('mux_512to1_8bit_2stages_64_8',1368,4169,'408.33','2'),('mux_512to1_8bit_3stages_128_2_2',1385,4153,'371.2','3'),('mux_512to1_8bit_3stages_16_8_4',1384,4393,'473.04','3'),('mux_512to1_8bit_3stages_32_4_4',1449,4265,'450.65','3'),('mux_512to1_8bit_3stages_64_4_2',1368,4185,'411.18','3'),('mux_512to1_8bit_3stages_8_8_8',1240,4681,'524.11','3'),('mux_512to1_8bit_4stages_16_4_4_2',1368,4441,'448.83','4'),('mux_512to1_8bit_4stages_32_4_2_2',1449,4281,'433.65','4'),('mux_512to1_8bit_4stages_64_2_2_2',1352,4217,'400.32','4'),('mux_512to1_8bit_4stages_8_4_4_4',1192,4777,'498.5','4'),('mux_512to1_8bit_5stages_16_4_2_2_2',1352,4473,'495.54','5'),('mux_512to1_8bit_5stages_32_2_2_2_2',1417,4345,'426.44','5'),('mux_512to1_8bit_5stages_4_4_4_4_2',1368,5465,'606.8','5'),('mux_512to1_8bit_5stages_8_4_4_2_2',1192,4793,'520.02','5'),('mux_512to1_8bit_6stages_16_2_2_2_2_2',1288,4601,'486.38','6'),('mux_512to1_8bit_6stages_4_4_4_2_2_2',1352,5497,'608.64','6'),('mux_512to1_8bit_6stages_8_4_2_2_2_2',1160,4857,'521.38','6'),('mux_512to1_8bit_7stages_4_4_2_2_2_2_2',1288,5625,'607.9','7'),('mux_512to1_8bit_7stages_8_2_2_2_2_2_2',1032,5113,'490.68','7'),('mux_512to1_8bit_8stages_4_2_2_2_2_2_2_2',1032,6137,'584.45','8'),('mux_512to1_8bit_9stages_2_2_2_2_2_2_2_2_2',8,8185,'573.07','9'),('mux_64to1_8bit_2stages_16_4',168,550,'615.38','2'),('mux_64to1_8bit_2stages_32_2',185,534,'557.1','2'),('mux_64to1_8bit_2stages_8_8',216,582,'652.32','2'),('mux_64to1_8bit_3stages_16_2_2',168,566,'630.52','3'),('mux_64to1_8bit_3stages_4_4_4',168,678,'822.37','3'),('mux_64to1_8bit_3stages_8_4_2',216,598,'700.77','3'),('mux_64to1_8bit_4stages_4_4_2_2',168,694,'833.33','4'),('mux_64to1_8bit_4stages_8_2_2_2',200,630,'663.13','4'),('mux_64to1_8bit_5stages_4_2_2_2_2',136,758,'773.4','5'),('mux_64to1_8bit_6stages_2_2_2_2_2_2',8,1014,'762.2','6'),('mux_8to1_8bit_2stages_4_2',24,83,'1011.12','2'),('mux_8to1_8bit_3stages_2_2_2',8,115,'1085.78','3');
/*!40000 ALTER TABLE `8_bit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-03 17:46:52
