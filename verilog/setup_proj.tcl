# Copyright (C) 1991-2013 Altera Corporation
# Your use of Altera Corporation's design tools, logic functions 
# and other software and tools, and its AMPP partner logic 
# functions, and any output files from any of the foregoing 
# (including device programming or simulation files), and any 
# associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License 
# Subscription Agreement, Altera MegaCore Function License 
# Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by 
# Altera or its authorized distributors.  Please refer to the 
# applicable agreement for further details.

# Quartus II: Generate Tcl File for Project
# File: mux_scan.tcl
# Generated on: Fri Feb 28 09:45:08 2014

# Load Quartus II Tcl Project package
package require ::quartus::project

set need_to_close_project 0
set make_assignments 1

# Check that the right project is open
if {[is_project_open]} {
	if {[string compare $quartus(project) [lindex $quartus(args) 0]]} {
		puts "Project [lindex $quartus(args) 0] is not open"
		set make_assignments 0
	}
} else {
	# Only open if not already open
	if {[project_exists [lindex $quartus(args) 0]]} {
		project_open -revision [lindex $quartus(args) 0] [lindex $quartus(args) 0]
	} else {
		project_new -revision [lindex $quartus(args) 0] [lindex $quartus(args) 0]
	}
	set need_to_close_project 1
}

# Make assignments
if {$make_assignments} {
	set_global_assignment -name FAMILY "Stratix IV"
	set_global_assignment -name DEVICE EP4SGX70HF35C2
	set_global_assignment -name TOP_LEVEL_ENTITY [lindex $quartus(args) 0]
	set_global_assignment -name ORIGINAL_QUARTUS_VERSION 13.0
	set_global_assignment -name PROJECT_CREATION_TIME_DATE "16:23:18  FEBRUARY 27, 2014"
	set_global_assignment -name LAST_QUARTUS_VERSION 13.0
	set_global_assignment -name PROJECT_OUTPUT_DIRECTORY output_files
	set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
	set_global_assignment -name MAX_CORE_JUNCTION_TEMP 85
	set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 256
	set_global_assignment -name EDA_SIMULATION_TOOL "ModelSim-Altera (VHDL)"
	set_global_assignment -name EDA_OUTPUT_DATA_FORMAT VHDL -section_id eda_simulation
	set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
	set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
	set_global_assignment -name PARTITION_NETLIST_TYPE SOURCE -section_id Top
	set_global_assignment -name PARTITION_FITTER_PRESERVATION_LEVEL PLACEMENT_AND_ROUTING -section_id Top
	set_global_assignment -name PARTITION_COLOR 16764057 -section_id Top
	set_global_assignment -name VERILOG_FILE submux.v
	set_global_assignment -name VERILOG_FILE [lindex $quartus(args) 0].v
	set_instance_assignment -name VIRTUAL_PIN ON -to clk 
	set_instance_assignment -name VIRTUAL_PIN ON -to in
	set_instance_assignment -name VIRTUAL_PIN ON -to sel
	set_instance_assignment -name VIRTUAL_PIN ON -to out
	set_instance_assignment -name PARTITION_HIERARCHY root_partition -to | -section_id Top

	# Commit assignments
	export_assignments

	# Close project
	if {$need_to_close_project} {
		project_close
	}
}

# liwei: an example of how to use this file

# create a project: mux_2to1_1stages_2
# quartus_sh -t setup_proj.tcl mux_2to1_1stages_2

# compile a project: mux_2to1_1stages_2
# quartus_sh --flow compile mux_2to1_1stages_2

# run timing analysis(optional)
# quartus_sta mux_2to1_1stages_2

